package com.ryctabo.spring.demo;

import com.ryctabo.spring.demo.rest.controller.UserResource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootDemoAppApplicationTests {

    @Autowired private UserResource controller;

    @Test
    void contextLoads() {
        Assertions.assertNotNull(controller);
    }
}
