/*
 * Copyright (c) 2020 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.ryctabo.spring.demo.rest.controller;

import com.ryctabo.spring.demo.domain.user.UserData;
import com.ryctabo.spring.demo.service.UserService;
import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Path("users")
@RestController
@AllArgsConstructor
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserService service;

    @GET
    public List<UserData> get() {
        return this.service.get();
    }

    @POST
    public Response post(@Valid @NotNull UserData data, @Context UriInfo uriInfo) {
        UserData user = this.service.add(data);
        URI location = uriInfo.getAbsolutePathBuilder().path(user.getId().toString()).build();
        return Response.created(location).entity(user).build();
    }

    @GET
    @Path("{id}")
    public UserData get(@PathParam("id") Long id) {
        return this.service.find(id);
    }

    @PUT
    @Path("{id}")
    public UserData put(@PathParam("id") Long id, @Valid @NotNull UserData data) {
        return this.service.update(id, data);
    }

    @DELETE
    @Path("{id}")
    public UserData delete(@PathParam("id") Long id) {
        return this.service.delete(id);
    }
}
