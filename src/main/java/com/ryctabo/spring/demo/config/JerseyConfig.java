/*
 * Copyright (c) 2020 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.ryctabo.spring.demo.config;

import com.ryctabo.spring.demo.rest.controller.UserResource;
import com.ryctabo.spring.demo.rest.filter.AcceptLanguageFilter;
import com.ryctabo.spring.demo.rest.filter.ContentTypeFilter;
import com.ryctabo.spring.demo.rest.filter.CorsResponseFilter;
import com.ryctabo.spring.demo.rest.filter.PoweredByResponseFilter;
import com.ryctabo.spring.demo.rest.mapper.ConstraintViolationExceptionMapper;
import com.ryctabo.spring.demo.rest.mapper.GenericExceptionMapper;
import com.ryctabo.spring.demo.rest.mapper.WebApplicationExceptionMapper;
import com.ryctabo.spring.demo.rest.resolver.ValidationConfigContextResolver;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Configuration
@ApplicationPath("v1")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        // Controllers
        register(UserResource.class);

        // Filters
        register(AcceptLanguageFilter.class);
        register(ContentTypeFilter.class);
        register(CorsResponseFilter.class);
        register(PoweredByResponseFilter.class);

        // Mappers
        register(ConstraintViolationExceptionMapper.class);
        register(GenericExceptionMapper.class);
        register(WebApplicationExceptionMapper.class);

        // Resolvers
        register(ValidationConfigContextResolver.class);
    }
}
