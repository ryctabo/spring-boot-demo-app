package com.ryctabo.spring.demo.config;

import com.ryctabo.spring.demo.domain.user.UserData;
import com.ryctabo.spring.demo.service.UserService;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Configuration
@AllArgsConstructor
public class UserConfigInitializer {

    private final UserService service;

    private final List<UserData> users;

    @PostConstruct
    public void setUp() {
        users.forEach(service::add);
    }
}
