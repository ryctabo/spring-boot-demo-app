package com.ryctabo.spring.demo.config;

import com.ryctabo.spring.demo.domain.user.UserData;
import java.time.LocalDate;
import java.time.Month;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Configuration
public class UserConfig {

    @Bean
    public UserData getRyctabo() {
        return UserData.builder()
                .username("ryctabo")
                .name("Gustavo Pacheco")
                .email("ryctabo@gmail.com")
                .birthdate(LocalDate.of(1993, Month.FEBRUARY, 11))
                .build();
    }

    @Bean
    public UserData getCari() {
        return UserData.builder()
                .username("carimile")
                .name("Carina Milena")
                .email("carinamilena1995@gmail.com")
                .birthdate(LocalDate.of(1995, Month.AUGUST, 27))
                .build();
    }
}
