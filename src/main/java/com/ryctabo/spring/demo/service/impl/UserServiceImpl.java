/*
 * Copyright (c) 2020 Gustavo Pacheco
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.ryctabo.spring.demo.service.impl;

import com.ryctabo.spring.demo.database.entity.User;
import com.ryctabo.spring.demo.database.repository.UserRepository;
import com.ryctabo.spring.demo.domain.user.UserData;
import com.ryctabo.spring.demo.service.UserService;
import com.ryctabo.spring.demo.service.converter.UserConverter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Gustavo Pacheco (ryctabo at gmail.com)
 * @version 1.0-SNAPSHOT
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    private final UserConverter converter;

    @Override
    public List<UserData> get() {
        return this.repository.findAll().stream()
                .map(converter::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserData find(Long id) {
        return this.repository
                .findById(id)
                .map(converter::convertToDto)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public UserData add(UserData data) {
        User user = this.converter.convertToEntity(data);
        user.setCreatedAt(LocalDateTime.now());
        return this.converter.convertToDto(repository.save(user));
    }

    @Override
    public UserData update(Long id, UserData data) {
        User user = this.repository.findById(id).orElseThrow(NotFoundException::new);

        user.setUsername(data.getUsername());
        user.setEmail(data.getEmail());
        user.setName(data.getName());
        user.setBirthdate(data.getBirthdate());
        user.setEnabled(data.getEnabled());
        user.setUpdatedAt(LocalDateTime.now());

        return this.converter.convertToDto(this.repository.save(user));
    }

    @Override
    public UserData delete(Long id) {
        User user = this.repository.findById(id).orElseThrow(NotFoundException::new);
        this.repository.delete(user);
        return this.converter.convertToDto(user);
    }
}
